#include <unistd.h>
#include <stdio.h>

int		s(char *a, char *b, int i)
{
	int		t;
	int		s;
	int		j;

	j = 0;
	while (b[j])
	{
		if (a[i] == b[j])
		{
			t = 0;
			s = 0;
			while(t < i)
			{
				if (a[t] == a[i])
					s = 1;
				t++;
			}
			if (s == 0)
				write(1, &a[i], 1);
			return (0);
		}
		j++;
	}
	return (0);
}

int		main(int ac, char **av)
{
	int		i;
	char	*a;
	char	*b;

	i = 0;
	a = av[1];
	b = av[2];
	if (ac == 3)
	{
		while (a[i])
		{
			s(a, b, i);
			i++;
		}
	}
	write(1, "\n", 1);
	return (0);
}
