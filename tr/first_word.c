#include <unistd.h>
#include <stdio.h>

int main (int ac,  char **av)
{
	int		i;
	char	*argv;

	argv = av[1];
	i = 0;
	if (ac == 2)
	{
		while (argv[i] == ' ' || argv[i] == '\t' || argv[i] == '\n' || argv[i] == '\v' ||
				argv[i] == '\f')
		{
			i++;
		}
		while(argv[i])
		{
			if(argv[i] == ' ' || argv[i] == '\t' || argv[i] == '\n' || argv[i] == '\v' ||
				argv[i] == '\f')
				return (0);
			else
			{
				write (1, &argv[i], 1);
				i++;
			}
		}
	}
	write (1, "\n", 1);
	return (0);
}
