#include <unistd.h>
#include <stdio.h>

int		pstr(char *str, int k)
{
	int		i;

	i = 0;
	while (i != k)
	{
		write(1, &str[i], 1);
		i++;
	}
	return (0);
}

int		s(char *s, char *res, int i, int j, int k, int n)
{
	while (res[k])
		k++;
	while (s[i])
	{
		n = 0;
		while (res[j])
		{
			if (s[i] == res[j])
				n = 1;
			j++;
		}
		if (n != 1)
		{
			res[k] = s[i];
			k++;
		}
		i++;
		j = 0;
	}
	res[k] = '\0';	
	return (k);
}

int		ch(char **av)
{
	int		i;
	int		j;
	int		n;
	int		k;
	char	*s1;
	char	*s2;
	char	res[4000];

	i = 0;
	j = 0;
	k = 0;
	n = 0;
	s1 = av[1];
	s2 = av[2];
	res[0] = s1[0];
	k = s(s1, res, i, j, k, n);
	k = s(s2, res, i, j, k, n);
	pstr(res, k);
	return (0);
}
int		main(int ac, char **av)
{
	if (ac == 3)
		ch(av);
	write(1, "\n", 1);
	return (0);
}
