#include <unistd.h>
#include <stdio.h>

int		pr(char **av, int l)
{
	while (av[1][l] != '\t' && av[1][l] != '\n' &&
				av[1][l] != '\v' && av[1][l] != '\f' &&
				av[1][l] != 32 && av[1][l] != '\0')
	{
		write(1, &av[1][l], 1);
		l++;
	}
	return (0);
}

int		ch(char **av)
{
	int		l;

	l = 0;
	while (av[1][l])
		l++;
	l--;
	while ((av[1][l] == '\t' || av[1][l] == '\n' ||
				av[1][l] == '\v' || av[1][l] == '\f' ||
				av[1][l] == ' ') && l != 0)
		l--;
	while (l != 0)
	{
		if (av[1][l] == '\t' || av[1][l] == '\n' ||
				av[1][l] == '\v' || av[1][l] == '\f' ||
				av[1][l] == ' ')
			break;
		else
			l--;
	}
	l++;
	pr (av, l);
	return (0);
}

int		main(int ac, char **av)
{
	ch (av);
	write(1, "\n", 1);
	return (0);
}
