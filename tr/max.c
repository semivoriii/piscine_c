#include <stdio.h>

int		max(int	*tab, unsigned int len)
{
	int		i;
	int		m;

	i = 0;
	m = tab[0];
	while(i < len)
	{
		if (m < tab[i])
			m = tab[i];
		i++;
	}
	return (m);
}

int		main(void)
{
	int a[6] = {4, 5, 3, 8, 4, 4};
	printf("%d\n", max(a, 6));
	return (0);
}
