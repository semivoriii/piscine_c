#include <unistd.h>

int		search(char **str)
{
	int		i;
	char	*s;
	char	a;
	char	b;

	i = 0;
	s = str[1];
	a = str[2][0];
	b = str[3][0];
	while (s[i])
	{
		if (s[i] == a)
			write(1, &b, 1);
		else
			write(1, &s[i], 1);
		i++;
	}
	return (0);
}

int		main(int ac, char **av)
{
	if (ac == 4 && av[2][1] == '\0' && av[3][1] == '\0')
	{
		search(av);
	}
	write (1, "\n", 1);
	return (0);
}
