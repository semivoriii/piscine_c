#include <stdio.h>
#include <stdlib.h>

int 	main(int ac, char **av)
{
	int		a = atoi(av[1]);
	int		b = atoi(av[3]);

	if(ac == 4)
	{
		if (av[2][0] == '+')
			printf("%d\n", a + b);
		if (av[2][0] == '-')
			printf("%d\n", a - b);
		if (av[2][0] == '*')
			printf("%d\n", a * b);
		if (av[2][0] == '/')
			printf("%d\n", a / b);
		if (av[2][0] == '%')
			printf("%d\n", a % b);
	}
	else
		printf("\n");
	return (0);
}
