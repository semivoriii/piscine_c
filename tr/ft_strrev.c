#include <stdio.h>

char	*ft_strrev(char *str)
{
	int		i;
	int		l;
	char	tmp;

	i = -1;
	l = 0;
	while(str[l])
		l++;
	while (i < l)
	{
		tmp = str[i];
		str[i] = str[l];
		str[l] = tmp;
		i++;
		l--;
	}
	return (str);
}

int		main(int ac, char **av)
{
	int		i;
	char	*str;

	i = 0;
	str = ft_strrev(av[1]);
	printf("%s\n", str);
	return (0);
}

