#include <unistd.h>
#include <stdio.h>

int		strl(char *s)
{
	int		i;

	i = 0;
	while(s[i])
		i++;
	return (i);
}

int		ps(char *s)
{
	int		i;

	i = 0;
	while (s[i])
	{
		write(1, &s[i], 1);
		i++;
	}
	return (0);
}

int		main(int ac, char **av)
{
	int		i;
	int		j;
	
	i = 0;
	j = 0;
	if (ac == 3)
	{
		while (av[1][i] && av[2][j])
		{
			if (av[1][i] == av[2][j])
				i++;
			j++;
		}
	if (strl(av[1]) == i)
		ps(av[1]);
	}
	write(1, "\n", 1);
	return (0);
}
