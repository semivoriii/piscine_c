#include <stdlib.h>
#include <stdio.h>

int		is_power_of_2(unsigned int n)
{
	if (n / 2 == 2 )
		return (1);
	else if (n % 2 == 0)
		return (is_power_of_2(n / 2));
	else
		return (0);
}

int		main(int ac, char **av)
{
	printf("%d\n", is_power_of_2(atoi(av[1])));
	return (0);
}
