#include <stdlib.h>
#include <unistd.h>

int		slen(char *av)
{
	int		i;

	i = 0;
	while(av[i])
		i++;
	return (i);
}

char	*ft_strdup(char *src)
{
	int		l;
	int		i;
	char	*str;

	i = 0;
	l = slen(src) + 1;
	str = (char *)malloc(sizeof(*str) * l);
	while (src[i])
	{
		str[i] = src[i];
		i++;
	}
	str[i] = '\0';
	return (str);
}

int		main(int ac, char **av)
{
	int		i;
	char	*s;

	i = 0;
	s = ft_strdup(av[1]);
	while(s[i])
	{
		write(1, &s[i], 1);
		i++;
	}
	return (0);
}
