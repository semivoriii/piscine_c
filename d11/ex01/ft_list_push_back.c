/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_back.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 16:06:42 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/25 17:20:48 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list	*t;

	t = *begin_list;
	if (t)
	{
		while (t->next)
			t = t->next;
		t->next = ft_create_elem(data);
	}
	else
		*begin_list = ft_create_elem(data);
}
