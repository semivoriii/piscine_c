/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 19:34:16 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/25 21:52:34 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdlib.h>

t_list	*ft_list_push_params(int ac, char **av)
{
	int		i;
	t_list	*l;
	t_list	*t;

	i = 1;
	t = NULL;
	while (i < ac)
	{
		l = ft_create_elem(av[i]);
		l->next = t;
		t = l;
		i++;
	}
	return (l);
}
