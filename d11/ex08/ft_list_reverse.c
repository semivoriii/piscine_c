/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_reverse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 22:23:35 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/25 22:51:01 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void	ft_list_reverse(t_list **begin_list)
{
	t_list	**h;
	t_list	*tmp;
	t_list	*p;

	h = **begin_list;
	tmp = NULL; 
	while (h)
	{
		p = h->next;
		h->next = tmp;
		tmp = h;
		h = p;
	}
}
