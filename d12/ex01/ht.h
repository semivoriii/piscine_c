/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ht.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/26 22:34:40 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/26 23:08:21 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HT_H
# define HT_H

# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <errno.h>

void	putstr(char *str);

void	error_h(char *argv);

int		error(char *argv);

int		print_read(char *buf);

int		ft_strcmp(char *s1, char *s2);

#endif
