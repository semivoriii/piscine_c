/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/26 18:23:13 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/26 23:05:50 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ht.h"

int		min(char *argv, int i, char *buf)
{
	int iii;

	iii = i;
	if (ft_strcmp(argv, "-") == 0)
	{
		(print_read(buf));
		iii++;
	}
	return (iii);
}

int		main(int argc, char **argv)
{
	int		f;
	int		r;
	int		i;
	char	buf[25];

	i = 1;
	if (argc == 1)
		return (print_read(buf));
	while (argv[i])
	{
		i = min(argv[i], i, buf);
		f = open(argv[i], O_RDWR);
		if (f == -1)
			return (error(argv[i]));
		while ((r = read(f, buf, 25)))
		{
			buf[r] = '\0';
			write(1, buf, r);
		}
		close(f);
		i++;
	}
	return (0);
}
