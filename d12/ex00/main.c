/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/26 17:33:15 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/26 22:07:55 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int		main(int argc, char **argv)
{
	int		f;
	int		r;
	char	buf[2049];

	if (argc < 2)
	{
		write(1, "File name missing.\n", 19);
		return (1);
	}
	if (argc > 2)
	{
		write(1, "Too many arguments.\n", 20);
		return (1);
	}
	f = open(argv[1], O_RDONLY);
	r = read(f, buf, 2048);
	buf[r] = '\0';
	write(1, buf, r);
	close(f);
	return (0);
}
