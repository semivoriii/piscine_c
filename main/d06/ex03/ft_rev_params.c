/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/18 20:14:28 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/18 20:17:41 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		main(int argc, char **argv)
{
	int i;
    int j;
    
	i = argc - 1;
	argc = 0;
	while (i > argc)
	{
		j = 0;
		while(argv[i][j])
			{
				ft_putchar(argv[i][j]);
				j++;
			}
			i--;
		ft_putchar('\n');
	}
	return(0);
}
