void	ft_putchar (char c);

int		main(int argc, char **argv)
{
	int		i;
	int		j;
	char	*c;

	i = 0;
	j = 1;
	if (argc > 1)
	{
		while(argv[j])
		{
			c = argv[j];
			while (c[i])
			{
				ft_putchar(c[i]);
				i++;
			}
			j++;
		ft_putchar('\n');
		}
	}
	return (0);
}
