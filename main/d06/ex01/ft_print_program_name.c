/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_program_name.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/18 16:31:23 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/18 20:19:17 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar (char c);

int		main(int argc, char **argv)
{
	int		i;
	char	*c;

	i = 0;
	c = argv[0];
	if (argc > 1)
	{
		while (c[i])
		{
			ft_putchar(c[i]);
			i++;
		}
		ft_putchar('\n');
	}
	return (0);
}
