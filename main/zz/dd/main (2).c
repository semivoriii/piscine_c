/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbondar <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/25 19:08:23 by dbondar           #+#    #+#             */
/*   Updated: 2018/07/25 19:11:10 by dbondar          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <stdio.h>

int main(int argc, char **argv)
{
	t_list *list = ft_list_push_params(argc,argv);
//	printf("%s",(char *)list);
	printf("%s",(char *)list->data);
	list = list->next;
	printf("%s",(char *)list->data);
	return (0);
}
