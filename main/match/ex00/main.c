#include <stdio.h>

int match(char *s1, char *s2);

char *c1 = "";
char *c2 = "";
int main()
{
	printf("%d, ozidalos %d\n", match("arcasha","a"), 0);
    printf("%d, ozidalos %d\n", match("adolf","a*"), 1);
    printf("%d, ozidalos %d\n", match("*a","*a"), 1);
    printf("%d, ozidalos %d\n", match("rainbow","*a*nbow"), 1);
    printf("%d, ozidalos %d\n", match("rainbow","*a**nbow"), 1);
    printf("%d, ozidalos %d\n", match("","*"), 1);
    printf("%d, ozidalos %d\n", match(c1, c2), 1);
}
