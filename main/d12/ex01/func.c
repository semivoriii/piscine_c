/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   func.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/26 22:25:59 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/26 22:38:25 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ht.h"

void	putstr(char *str)
{
	int		i;

	i = 0;
	while (str[i] != '\0')
	{
		write(1, &str[i], 1);
		i++;
	}
	write(1, "\n", 1);
}

void	error_h(char *argv)
{
	int		i;

	i = 0;
	write(1, "cat: ", 5);
	while (argv[i])
		write(1, &argv[i++], 1);
	write(1, ": ", 2);
}

int		error(char *argv)
{
	if (errno == ENOENT)
	{
		error_h(argv);
		putstr("No such file or directory.");
	}
	else if (errno == EISDIR)
	{
		error_h(argv);
		putstr("Is a directory.");
	}
	else if (errno == EACCES)
	{
		error_h(argv);
		putstr("Permission denied.");
	}
	return (0);
}

int		print_read(char *buf)
{
	int		r;

	while ((r = read(0, buf, 25)))
	{
		buf[r] = '\0';
		write(1, buf, r);
	}
	return (1);
}

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] != '\0' && s2[i] != '\0' && s1[i] == s2[i])
	{
		i++;
	}
	return (s1[i] - s2[i]);
}
