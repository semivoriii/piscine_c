/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/19 17:42:20 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/19 18:01:39 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int		len;
	int		i;
	int		j;

	if (min >= max)
		return (0);
	len = max - min;
	i = 0;
	j = 0;
	if (range)
	{
		while (i < 1)
		{
			range[i] = (int*)malloc(sizeof(**range) * len);
			while (min < max)
			{
				range[i][j] = min;
				min++;
				j++;
			}
			i++;
		}
		return (len);
	}
	return (0);
}
