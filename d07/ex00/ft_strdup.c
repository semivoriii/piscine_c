/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/19 16:12:42 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/19 17:16:09 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	int		len;
	char	*res;
	int		i;

	len = 0;
	while (src[len])
		len++;
	res = (char*)malloc(sizeof(*res) * (len + 1));
	i = 0;
	if (res)
	{
		while (src[i])
		{
			res[i] = src[i];
			i++;
		}
		res[i] = '\0';
		return (res);
	}
	return (0);
}
