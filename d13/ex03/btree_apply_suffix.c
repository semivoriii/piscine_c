/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_apply_suffix.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 22:14:13 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/27 22:28:00 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_apply_suffix(t_btree *root, void (*applyf)(void *))
{
	t_btree	*n;

	n = root;
	if (n)
	{
		if (n->left)
			btree_apply_suffix(n->left, applyf);
		if (n->right)
			btree_apply_suffix(n->right, applyf);
		applyf(n->item);
	}
}
