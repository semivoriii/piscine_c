/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_apply_infix.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 22:03:31 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/27 22:23:53 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_apply_infix(t_btree *root, void (*applyf)(void *))
{
	t_btree *n;

	n = root;
	if (n)
	{
		if (n->left)
			btree_apply_infix(n->left, applyf);
		applyf(n->item);
		if (n->right)
			btree_apply_infix(n->right, applyf);
	}
}
