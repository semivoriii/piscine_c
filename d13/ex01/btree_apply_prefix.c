/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_apply_prefix.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 20:35:43 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/27 21:47:33 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_apply_prefix(t_btree *root, void (*applyf)(void *))
{
	t_btree	*n;

	n = root;
	if (n)
	{
		applyf(n->item);
		if (n->left)
			btree_apply_prefix(n->left, applyf);
		if (n->right)
			btree_apply_prefix(n->right, applyf);
	}
}
