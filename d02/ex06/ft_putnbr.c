/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/12 20:16:24 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/12 21:47:36 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putnbr(int nb)
{
	int		i;
	char	*lowest_int;

	lowest_int = "-2147483648";
	i = 0;
	if (nb == -2147483648)
	{
		while (lowest_int[i] != '\0')
		{
			ft_putchar(lowest_int[i]);
			i++;
		}
		return ;
	}
	if (nb < 0 && nb != -2147483648)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb > 9 && nb != -2147483648)
	{
		ft_putnbr(nb / 10);
	}
	ft_putchar(nb % 10 + '0');
}
