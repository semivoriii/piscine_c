/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vosadchy <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/17 20:57:39 by vosadchy          #+#    #+#             */
/*   Updated: 2018/07/17 21:48:12 by vosadchy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[0] >= 'a' && str[0] <= 'z')
		{
			str[i] = str[i] - 32;
			i++;
		}
		if (str[i] < '0' && str[i] > '9' && str[i] < 'A' && str[i] > 'Z' &&
				str[i] < 'a' && str[i] > 'z')
		{
			i++;
			if (str[i] >= 'a' && str[i] <= 'z')
			{
				str[i] = str[i] - 32;
				i++;
			}
		}
		i++;
	}
	return (str);
}
